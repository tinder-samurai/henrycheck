package dev.tindersamurai.henrychek.starter;


import dev.tindersamurai.henrychek.repositories.RepositorySource;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component @Slf4j
public class DefaultStarter implements ApplicationStarter {

	private final RepositorySource sourceProvider;

	@Autowired
	public DefaultStarter(RepositorySource sourceProvider) {
		this.sourceProvider = sourceProvider;
	}

	@Override
	public void onStartUp(ApplicationStartedEvent event) {

		val latestVersion = sourceProvider.resolveLatestVersion();
		log.info("LATEST VERSION: {}", latestVersion);

		log.info("DOWNLOADING ARTIFACTS...");

		val file = sourceProvider.getLatestVersion();
		log.info("ARTIFACT: {}", file);

		startApp(file);
	}

	private static void startApp(File file) {
		log.info("START UP APPLICATION: {}", file);

		//noinspection ResultOfMethodCallIgnored
		file.setReadable(true);
		//noinspection ResultOfMethodCallIgnored
		file.setWritable(true);
		//noinspection ResultOfMethodCallIgnored
		file.setExecutable(true);


		val commandLine = new CommandLine("java");
		commandLine.addArguments(new String[] {"-jar ", file.getPath()});
		val executor = new DefaultExecutor();
		executor.setExitValue(423);

		try {
			executor.execute(commandLine);
		} catch (IOException e) {
			log.error("EXECUTION ERROR", e);
		}
	}
}
