package dev.tindersamurai.henrychek.repositories;

import java.io.File;

public interface RepositorySource {

	String resolveLatestVersion();

	String resolveLatestMD5();

	File getLatestVersion();


}
