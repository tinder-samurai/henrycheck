package dev.tindersamurai.henrychek.repositories.github;

import com.google.common.io.BaseEncoding;
import dev.tindersamurai.henrychek.repositories.RepositorySource;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;

@Service @Slf4j
public class GitHubRepository implements RepositorySource {

	private static final String WORKING_ARTIFACT_PATH = ".internal/artifact.jar";
	private final Environment environment;

	@Autowired
	public GitHubRepository(Environment environment) {
		this.environment = environment;
	}

	@Override
	public String resolveLatestVersion() {
		log.debug("resolveLatestVersion");
		return environment.getRequiredProperty("henrycheck.repository.release.url");
	}

	@Override
	public String resolveLatestMD5() {
		log.debug("resolveLatestMD5");
		return environment.getRequiredProperty("henrycheck.repository.release.md5");
	}

	private static byte[] calcMD5(String p) {
		try {
			val path = Paths.get(p);
			if (!path.toFile().exists()) return new byte[0];
			return MessageDigest.getInstance("MD5").digest(Files.readAllBytes(path));
		} catch (Exception e) {
			log.error("Cannot calculate md5 sum", e);
			return new byte[0];
		}
	}

	@Override
	public File getLatestVersion() {
		log.debug("getLatestVersion");

		val path = Paths.get(WORKING_ARTIFACT_PATH);
		val file = path.toFile();

		val currentMD5 = BaseEncoding.base16().encode(calcMD5(WORKING_ARTIFACT_PATH));
		val latestMD5 = resolveLatestMD5();

		log.info("C_MD5: {}", currentMD5);
		log.info("R_MD5: {}", latestMD5);

		if (currentMD5.equalsIgnoreCase(latestMD5)) {
			log.debug("NO NEED DOWNLOAD");
			return file;
		}

		try {
			val in = new URL(resolveLatestVersion()).openStream();
			//noinspection ResultOfMethodCallIgnored
			file.mkdirs();

			Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
			return file;
		} catch (Exception e) {
			log.error("CANNOT DOWNLOAD LATEST VERSION", e);
			System.exit(0);
			return null;
		}
	}
}
